import * as moment from "moment";
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";
import { IBook } from "../../model/book";
import { BookControls, dateTemplate, IBaseBook, IBookControls } from "./book-controls";
import { FormGroup } from "@angular/forms";
import { combineLatest, Observable } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import { UnSubscribeService } from "../../service/un-subscribe.service";

export enum FORM_STATUS {
  VALID = "VALID",
  INVALID = "INVALID",
  PENDING = "PENDING",
  DISABLED = "DISABLED",
}

export enum FORM_EVENTS {
  TOUCH = "TOUCH",
  RESET = "RESET",
}

export interface IBookChange {
  status: FORM_STATUS;
  book: IBook;
}

@Component({
  selector: "app-edit-book-form",
  templateUrl: "./edit-book-form.component.html",
  styleUrls: ["./edit-book-form.component.styl"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [UnSubscribeService],
})
export class EditBookFormComponent implements OnInit, OnChanges {
  @Input() book: IBook;
  @Input() isEdit: boolean;
  @Input() events$: Observable<any>;
  @Output() bookFormChange = new EventEmitter<IBookChange>();

  public bookControls: IBookControls;
  public bookFormGroup: FormGroup;

  constructor(private unsubscribeService: UnSubscribeService) {}

  ngOnInit() {
    this.bookControls = new BookControls(this.book);
    this.bookFormGroup = new FormGroup({ ...this.bookControls });

    this.isEdit ? this.bookFormGroup.enable() : this.bookFormGroup.disable();

    combineLatest(
      this.bookFormGroup.valueChanges.pipe(
        map(
          (data: IBaseBook) => ({ ...data, id: this.book.id, date: moment(data.date, dateTemplate).toDate() } as IBook)
        )
      ),
      this.bookFormGroup.statusChanges
    )
      .pipe(takeUntil(this.unsubscribeService))
      .subscribe(([book, status]) => {
        console.log("combineLatest", book, status), this.bookFormChange.emit({ status, book });
      });

    this.events$.subscribe((event) => {
      console.log("event", event);
      switch (event) {
        case FORM_EVENTS.TOUCH:
          this.bookFormGroup.markAllAsTouched();
          break;
        case FORM_EVENTS.RESET:
          this.bookFormGroup.reset();
          break;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("changes", changes);

    if (changes.isEdit && this.bookFormGroup) {
      changes.isEdit.currentValue ? this.bookFormGroup.enable() : this.bookFormGroup.disable();
    }
  }

  public onUpLoadAvatar(files: FileList) {
    console.log("files", files);
    const file: File = files.item(0);
    const reader = new FileReader();
    reader.onloadend = () => {
      let base64String;
      // use a regex to remove data url part
      if (typeof reader.result === "string") {
        base64String = reader.result.replace("data:", "").replace(/^.+,/, "");
      }
      this.bookControls.image.setValue(base64String);
    };
    reader.readAsDataURL(file);
  }

  public rest() {
    this.bookFormGroup.reset();
  }
}
