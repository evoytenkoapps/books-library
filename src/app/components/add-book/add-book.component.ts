import { Component, OnInit } from "@angular/core";
import { BookService } from "../../service/book.service";
import { IBook } from "../../model/book";
import { FORM_EVENTS, FORM_STATUS, IBookChange } from "../edit-book-form/edit-book-form.component";
import { Subject } from "rxjs";

@Component({
  selector: "app-add-book",
  templateUrl: "./add-book.component.html",
  styleUrls: ["./add-book.component.styl"],
})
export class AddBookComponent implements OnInit {
  public bookChange: IBookChange;
  public events$ = new Subject<FORM_EVENTS>();
  public book: IBook;
  public isEdit = true;

  constructor(private dataService: BookService) {}

  ngOnInit() {
    this.book = {
      note: "",
      feedback: "",
      rating: null,
      pages: null,
      date: new Date(),
      isbn: null,
      publisher: "",
      author: "",
      title: "",
      image: null,
      name: "",
      id: +new Date(),
    };
  }

  public onBookChange(book: IBookChange) {
    this.bookChange = book;
  }

  public addBook() {
    if (this.bookChange.status === FORM_STATUS.VALID) {
      this.dataService.add(this.bookChange.book);
      this.events$.next(FORM_EVENTS.RESET);
    } else {
      this.events$.next(FORM_EVENTS.TOUCH);
    }
  }
}
