import { Component, EventEmitter, OnInit } from "@angular/core";
import { BookService } from "../../service/book.service";
import { IBook } from "../../model/book";
import { Router } from "@angular/router";
import { Observable, Subject } from "rxjs";
import { map, tap } from "rxjs/operators";
import { FORM_EVENTS, FORM_STATUS, IBookChange } from "../edit-book-form/edit-book-form.component";

@Component({
  selector: "app-edit-book",
  templateUrl: "./edit-book.component.html",
  styleUrls: ["./edit-book.component.styl"],
})
export class EditBookComponent implements OnInit {
  public book$: Observable<IBook>;
  public isEdit = false;
  public bookChange: IBookChange;
  public events$ = new Subject<FORM_EVENTS>();

  constructor(private bookService: BookService, private router: Router) {}

  ngOnInit() {
    this.book$ = this.bookService.storage$.pipe(
      map((books) => books.find((book) => book.id === +this.router.url.split("/")[2])),
      tap((book) => console.log("bookis", book))
    );
  }

  public edit() {
    this.isEdit = true;
  }

  public onBookChange(data: IBookChange) {
    this.bookChange = data;
  }

  public save() {
    if (this.bookChange.status === FORM_STATUS.VALID) {
      console.log("save", this.bookChange.book);
      this.isEdit = false;
      this.bookService.saveBook(this.bookChange.book);
    } else {
      this.events$.next(FORM_EVENTS.TOUCH);
    }
  }
}
