import { Injectable, OnDestroy } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable()
export class UnSubscribeService extends Observable<void> implements OnDestroy {
  private readonly unsubscribe$ = new Subject<void>();

  constructor() {
    super((subscriber) => this.unsubscribe$.subscribe(subscriber));
  }

  ngOnDestroy() {
    console.log("UnSubscribeService ngOnDestroy");
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
